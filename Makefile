FILE=van-gogh

default: $(FILE).pdf

$(FILE).pdf: $(FILE).tex $(FILE).bib
	pdflatex $(FILE)
	bibtex $(FILE)
	pdflatex $(FILE)
	pdflatex $(FILE)

biblio:
	perl -pi -e 's/\\sc//g' evolution.bbl

$(FILE).ps: $(FILE).dvi
	dvips -t a4 -Ppdf -G0 -o $@ $<

clean:
	rm -f *.log *.blg *.bbl *~ *.aux *.dvi *.pdf *.ps *.lbl




wc: 	$(FILE).ps
	ps2ascii $(FILE).ps | wc

#$(FILE).pdf: $(FILE).ps
#	ps2pdf -dCompatibilityLevel=1.3 -dMaxSubsetPct=100 \
#	  -dSubsetFonts=true -dEmbedAllFonts=true \
#	  -dColorImageResolution=300 -dColorImageDownsampleType=/Bicubic\
#	  -dGrayImageResolution=300 \
#	  -dMonoImageResolution=600 \
#	  -dAutoFilterColorImages=false -dAutoFilterGrayImages=false \
#	  -dColorImageFilter=/FlateEncode -dGrayImageFilter=/FlateEncode \
#	  -dMonoImageFilter=/FlateEncode  $< $@
#
