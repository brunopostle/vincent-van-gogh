\documentclass{egpubl}

%\usepackage{cae2010}
\usepackage{subfigure}
\usepackage{graphics}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{xspace}
\usepackage[utf8]{inputenc}

\usepackage{balance}
\WsPaper

\ifpdf \usepackage[pdftex]{graphicx} \pdfcompresslevel=9
\else \usepackage[dvips]{graphicx} \fi

\PrintedOrElectronic

\usepackage{t1enc,dfadobe}

\usepackage{egweblnk}
\usepackage{cite}

% this helps with figure placement...
\usepackage{stfloats}

\newlength{\imageWidth}
\setlength{\imageWidth}{2.75in}
\newlength{\imageWidthHalf}
\setlength{\imageWidthHalf}{1.375in}

\setlength{\parskip}{1.5mm}

% Uncomment if your document adheres to LaTeX2e recommendations.
\let\rm=\rmfamily    \let\sf=\sffamily    \let\tt=\ttfamily
\let\it=\itshape     \let\sl=\slshape     \let\sc=\scshape
\let\bf=\bfseries

% end of prologue

\newcommand{\degree}{\ensuremath{^\circ}}

% \newcommand{\urlSupp}{{\small\url{http://vedutismo.net/Pannini/}}}

\begin{document}

\title[van Gogh's perspective machine] {The Perspective Machine of Vincent van Gogh}

\author[Bruno Postle et al.] {Bruno Postle$^{1}$, Thomas K. Sharpless$^{2}$, and Daniel M. German$^{3}$
\\
$^1$bruno@postle.net \quad\quad
$^2$tksharpless@gmail.com \quad\quad
$^3$dmg@uvic.ca, Dept. of Computer Science, University of Victoria.
}

\bibliographystyle{eg-alpha}

\maketitle

\begin{abstract}

In this paper we demonstrate a likely usage of van Gogh's unique perspective
frame based on surviving records, we also show how this method produces some of
the characteristics of the artist's distinctive perspective, we also note that
this perspective is equivalent to the General Pannini family of projections and
illustrate how it can be reproduced photographically.

\end{abstract}

\section{Introduction}
\label{sec:introduction}

Vincent Willem van Gogh (1853 - 1890) was a celebrated post-impressionist
artist noted for pioneering colour in his art, but also for an apparent
disregard of traditional perspective technique.  Regarding his perspective,
previous work suggests that van Gogh did TODO \cite{heelan1972} or TODO
\cite{heelan1998}, however we believe that TODO.

\section{Characteristics of van Gogh's perspective}
\label{sec:characteristics}

In February 1888 van Gogh moved from Paris to Arles in the south of France
where he painted his most distinctive work and developed his characteristic
perspective style. Here we illustrate two scenes that the artist painted that
year multiple times, The Night Café (two versions from September 1888, Figures
\ref{fig:nightcafe1}, \ref{fig:nightcafe2}) and Bedroom in Arles (three
versions painted between October 1888 and September 1889, Figures
\ref{fig:bedroom1}, \ref{fig:bedroom2}, \ref{fig:bedroom3}).

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image06.jpg}
  \caption{ \emph{The Night Café}, oil on canvas, Vincent van Gogh (September 1888).}
  \vspace{-3mm}
  \label{fig:nightcafe1}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image10.jpg}

  \caption{ \emph{The Night Café}, watercolour, Vincent van Gogh (1888). This
is a copy made by the artist as a design for a Japanese woodcut print.}

  \vspace{-3mm}
  \label{fig:nightcafe2}
\end{figure}

We would like to draw attention to three aspects common to these paintings,
the first two are typical of most perspectives, but it is the last that
makes them stand out:

1. A radial composition with lines parallel to the view direction converging on
a vanishing point.

2. Vertical features are drawn parallel to the sides of the canvas.

3. Horizontal features, that in three dimensional space would be orthogonal to
the other two sets of lines, curve and converge both to the left and to the
right of the picture.

See Figures \ref{fig:bedroom-overlay}, \ref{fig:cafe-overlay-1},
\ref{fig:cafe-overlay-2}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image09.jpg}
  \caption{ \emph{Bedroom in Arles}, oil on canvas, Vincent van Gogh (October 1888).}
  \vspace{-3mm}
  \label{fig:bedroom1}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image13.jpg}
  \caption{ \emph{Bedroom in Arles}, oil on canvas, Vincent van Gogh (September 1889).}
  \vspace{-3mm}
  \label{fig:bedroom2}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/Vincent_Willem_van_Gogh_135.jpg}
  \caption{ \emph{Bedroom in Arles}, oil on canvas, Vincent van Gogh (September 1889).}
  \vspace{-3mm}
  \label{fig:bedroom3}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/Ward in the Hospital at Arles.jpg}
  \caption{ \emph{Ward in the Hospital at Arles}, oil on canvas, Vincent van Gogh (April 1889).}
  \vspace{-3mm}
  \label{fig:ward}
\end{figure}

Compare the Bedroom paintings with a sketch in a letter to the artist Paul
Gaugin in October 1888 Figure \ref{fig:bedroom-sketch}, the artist here has shown the
horizontal features using a conventional perspective with no convergence.
The graph paper used probably has something to do with this.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image05.jpg}
  \caption{ \emph{Bedroom in Arles}, Letter to Paul Gaugin 706/B22, 17th October 1888, Vincent van Gogh.}
  \vspace{-3mm}
  \label{fig:bedroom-sketch}
\end{figure}

\section{Comparison with classical perspective}
\label{sec:comparison}

The rules of renaissance perspective form a projective geometry, the
principle is often described by showing an artist looking through a window and
tracing the scene onto the glass, well illustrated by the engraving by Albrecht
Dürer Figure \ref{fig:durer1}. This technique produces a 'true' rectilinear
projection - Which is incidentally the same projection as produced by camera
obscura, camera lucida, pinhole and 'normal' photographic lenses.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image03.jpg}

  \caption{ \emph{De Symmetria and Underweysung der Messung}, Albrecht Dürer
(1532).  The perspective device here is a sheet of glass that the artist paints
directly, copying the scene on the other side of the glass.  Such a system
requires that the artist keeps his eye in the same position throughout the
whole process, so the machine necessariliy provides a pin or eyepiece with an
adjustable position.}

  \vspace{-3mm}
  \label{fig:durer1}
\end{figure}

Rectilinear projection images preserve straight lines, i.e. any features that
are straight in three dimensional space will be represented as straight lines
on the canvas. Rectilinear projection has its drawbacks, wide angle-of-view
pictures show extreme distortion around the periphery, indeed it is only
possible to represent a scene with an angle-of-view less than 180\degree,  in
practice angles-of-view greater than about 90\degree produce unacceptable distortion.

(TODO an example of unacceptable distortion here)

Van Gogh in his own words describes his early education in perspective
techniques as essential, he read Armand Cassagne's books \cite{Cassagne1866}
and explored traditional perspective with his early Dutch paintings and
drawings, an example is Rooftops, View from the Atelier (1882) Figure
\ref{fig:rooftops} which shows features of a conventional rectilinear
perspective.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image01.jpg}

  \caption{ \emph{Rooftops, View from the Atelier}, Vincent van Gogh (1882).
This painting shows a mastery of conventional rectilinear perspective.}

  \vspace{-3mm}
  \label{fig:rooftops}
\end{figure}

\section{A classical perspective machine}
\label{sec:classical}

There are several techniques for assembling a rectilinear perspective, the one
we wish to highlight here involves the use of a 'perspective machine' or
'perspective frame'. The concept is similar to the window analogy (Figure
\ref{fig:durer1}) described above, a rectangular frame with the same
proportions as the canvas is suspended in front of the artist, this frame is
strung with a square grid made from fine thread, a grid with the same
proportions is also drawn on the canvas by the artist. Provided the artist can
keep their eye in the same position, all they need to do is copy one square at
a time from the scene to the canvas, and the result is a true rectilinear
representation of the scene.  Again Albrecht Dürer illustrated this principle
well Figure \ref{fig:durer2}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image12.jpg}

  \caption{ \emph{De Symmetria and Underweysung der Messung}, Albrecht Dürer
(1532). An alternative perspective machine, the rectangular frame and
adjustable eyepiece are the same as Figure \ref{fig:durer1}, however, instead
of drawing directly on a sheet of glass placed in the frame, a square grid is
strung in the frame and duplicated on the paper where the final picture is
drawn.}

  \vspace{-3mm}
  \label{fig:durer2}
\end{figure}

Note that the requirement for the artist to keep one eye in the same
position relative to the frame is onerous, a perspective machine typically
includes an eyepiece or fixed pin so the artist can keep the same eye
position, though this is sufficiently uncomfortable that modern artists
rarely use such machines.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/drac3l.jpg}

  \caption{ \emph{Production still from 'The Draughtsman's Contract'}, Peter
Greenaway (1982).  Note the eyepiece necessary to keep the artist's eye in the
same position, this differs only slightly from Dürer's Figure \ref{fig:durer2}
engraving which featurs a pin instead.}

  \vspace{-3mm}
  \label{fig:draughtsmans}
\end{figure}

\section{Van Gogh's perspective frame}
\label{sec:frame}

We know from the artist's correspondence with his brother Theo that van
Gogh had two custom perspective frames built in 1882, the letters contain
diagrams illustrating the frame but little written description of the
technique.

The first version of the frame was built in June 1882:

\begin{quotation}

" ... I had more expenses in connection with the study of perspective and
proportion for an instrument described in a work by Albrecht Dürer and used by
the Dutchmen of old. It makes it possible to compare the proportions of objects
close at hand with those on a plane further away, in cases where construction
according to the rules of perspective isn't feasible. Which, if you do it by
eye, will always come out wrong, unless you're very experienced and skilled.

I didn't manage to make the thing the first time around, but I succeeded in the
end after trying for a long time with the aid of the carpenter and the smith.
And I think that with more work I can get much better results still."

Letter 235, June 1882 \cite{let235}
\end{quotation}

Two months later, van Gogh is making a second version of the frame.  van Gogh
illustrated this with a sketch showing how he intends to use this new frame
Figure \ref{fig:frame-in-use}, the artist is standing with the canvas in one
arm, the frame is positioned with the centre at eye level, and there is no sign
of any kind of eyepiece. Note that this sketch also shows a distinctive
radial grid of string.

\begin{quotation}

"I'll start with small things -- but before the summer ends I hope to practise
bigger sketches in charcoal with an eye to painting in a rather larger format
later.  This is why I'm having a new and, I hope, better perspective frame
made, which will stand firmly on two legs in uneven ground like the dunes.

Like this, for example. (Figure \ref{fig:frame-in-use})"

Letter 253, August 1882 \cite{let253}
\end{quotation}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image11.jpg}

  \caption{ \emph{Sketch of perspective frame in use}, Letter 253 to Theo van
Gogh, Vincent van Gogh (August 1882). This illustrates the ability to set up
the frame in difficult locations, notice how the two posts are set at different
heights to accomodate the slope of the ground. See also enlarged detail Figure
\ref{fig:frame-in-use-detail}.}

  \vspace{-3mm}
  \label{fig:frame-in-use}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image11-detail.jpg}

  \caption{ \emph{Sketch of perspective frame in use, detail}, Letter 253 to
Theo van Gogh, Vincent van Gogh (August 1882). This detail from Figure
\ref{fig:frame-in-use} shows the distinctive radial grid.  The artist is
standing with the sketch board in one hand, there is no eyepiece, however the
centre of the radial grid is placed exactly at eyelevel (i.e. note that the
artists eye, the horizon and the grid centre are at the same vertical height).}

  \vspace{-3mm}
  \label{fig:frame-in-use-detail}
\end{figure}

In the next letter to Theo, Vincent describes the frame in more detail:

\begin{quotation}

"In my last letter you'll have found a little scratch of that perspective frame
Figure \ref{fig:frame-in-use}. I've just come back from the blacksmith, who has put
iron spikes on the legs and iron corners on the frame.

It consists of two long legs: (Figure \ref{fig:frame-detail})

The frame is fixed to them by means of strong wooden pegs, either horizontally
or vertically.

The result is that on the beach or in a meadow or a field you have a view as if
through a window. The perpendicular and horizontal lines of the frame, together
with the diagonals and the cross -- or otherwise a grid of squares -- provide a
clear guide to some of the principal features, so that one can make a drawing
with a firm hand, setting out the broad outlines and proportions. 1 Assuming,
that is, that one has a feeling for perspective and an understanding of why and
how perspective appears to change the direction of lines and the size of masses
and planes. Without that, the frame is little or no help, and makes your head
spin when you look through it.

I expect you can imagine how delightful it is to train this view-finder on the
sea, on the green fields -- or in the winter on snow-covered land or in the
autumn on the fantastic network of thin and thick trunks and branches, or on a
stormy sky.

With CONSIDERABLE practice and with lengthy practice, it enables one to draw at
lightning speed and, once the lines are fixed, to paint at lightning speed.

It's in fact especially good for painting, because a brush must be used for
sky, ground, sea. Or, rather, to render them through drawing alone, it's
necessary to know and feel how to work with the brush."

Letter 254, August 1882 \cite{let254}

\end{quotation}

Here we show van Gogh's sketch illustrating the construction Figure \ref{fig:frame-detail},
it consists of a rectangular frame that spans between two detachable posts, the
posts are secured with pegs and holes that allow height adjustment and that can
accommodate variable terrain. Note also the diagonal and orthogonal grid of
string that intersects in the centre.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image08.jpg}

  \caption{ \emph{Detail sketch of perspective frame showing grid}, Letter 254
to Theo van Gogh, Vincent van Gogh (August 1882).  Note the distinctive radial
grid of string. Instead of a tripod or table, the frame is supported by two
sharpened posts which can be driven vertically into soft or uneven ground.
Each post features a series of peg holes which are matched by peg holes in the
frame, these allow the frame height to be adjustable.  The frame can also be
placed in portrait or landscape orientation.}

  \vspace{-3mm}
  \label{fig:frame-detail}
\end{figure}

We know that van Gogh continued to use this frame, as he states in March
1888 after arriving in Arles:

\begin{quotation}
"I made my last three studies with the help of the perspective frame you
know about. I attach importance to the use of the frame, because it doesn't
seem unlikely to me that several artists will use it in the not too distant
future, just as the old German and Italian painters, certainly, and, I'm
inclined to believe, the Flemish artists too, used it.

The modern use of this tool may differ from the use people made of it in the
past -- but -- isn't it also true that with the process of painting in oils we
nowadays achieve very different effects from those of the inventors of the
process, J. and Hubert van Eyck? This is to say that I still hope not to
work for myself alone. I believe in the absolute necessity of a new art of
colour, of drawing and -- of the artistic life. And if we work in that faith,
it seems to me that there's a chance that our hopes won't be in vain."

Letter 585, March 1888 \cite{let585}
\end{quotation}

TODO \cite{Wright83}

Unfortunately, the principle of van Gogh's perspective frame died with him two
years later.  Though we believe that it is possible to deduce it from this
evidence left behind in letters and the paintings themselves.

\section{Proposed usage}
\label{sec:proposed}

Presumably the grid was used in the same basic way as with a traditional
perspective machine, i.e. the artist drew this diagonal grid on the canvas and
copied the scene segment by segment by looking at the scene through the frame.

\section{Tests with reproducing the technique}
\label{sec:tests}

With our tests using a reproduction of the apparatus, the advantage of the
diagonal grid becomes obvious - The grid of string looks the same and
segments the scene in the same way whatever distance you are from the frame.
There is no need to use an eyepiece, alignment can be retained just by the
artist moving her head sideways such that a chosen feature in the scene
always lines-up with the intersection of the grid.

A consequence of this is that the edges of the frame do not coincide with
the edges of the canvas, the artist has to fill each segment from the centre
outwards and can't easily compose the picture before they start.

In the photos you can see our test apparatus simply consists of a window with
van Gogh's grid drawn on the glass with a permanent marker pen. You can see
also that the grid segments the scene identically when viewed from different
distances (the grid has been highlighted to make it more visible for print).

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image00.jpg}

  \caption{ \emph{Test grid drawn on a window}, by the author (2010).  This
differs from van Gogh's frame which has string forming the grid, note that
unlike a traditional perspective machine where the frame is part of the grid,
in this case the frame is irrelevant to the process.}

  \vspace{-3mm}
  \label{fig:test-grid}
\end{figure}

(TODO more photos showing grid from different distances)

The sketch by one of the authors was quite straightforward to produce, the
discipline  of keeping the grid aligned with the view is actually very
natural and not at all restrictive.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image14.jpg}

  \caption{ \emph{Sketch drawn with test grid}, by the author (2010). Created
using the grid drawn on a window shown above Figure \ref{fig:test-grid}.}

  \vspace{-3mm}
  \label{fig:test-sketch}
\end{figure}

A consequence of the radial grid is that whereas with a rectangular grid you
have no choice but to produce a precise rectilinear perspective, with the
radial grid, the angular positions of features can be placed accurately, but
the radial distance is something that still needs to be judged by eye -- since
the grid places no restriction on this. An assumption of this paper is that an
artist will generally attempt to draw 'conformally', i.e. she will attempt to
draw people and buildings with a 'true' proportion rather than stretching
objects at the periphery like a rectilinear projection photograph. Each of
these wedges provided by the frame can be easily filled conformally by the
artist and the resulting image has some interesting properties:

1. Radial features are preserved due to the grid in the frame.

2.  Lateral features are drawn curved due to the increasing horizontal
compression at the edges required to maintain minimal local distortion in
combination with the preservation of angular position.

3. An extremely large angle-of-view is possible, the authors sketch is
approximately 120\degree wide, something that isn't practical to achieve with a
rectangular gridded perspective frame.

\section{Pannini perspective}
\label{sec:pannini}

There is of course a projective geometry that matches these properties, this is
provided by the 'General Pannini' \cite{sharpless2010cae} projection named
after the eighteenth century vedutismo artist (Giovanni Paolo Pannini 1691 --
1765). Whereas Pannini himself used the orthographic variation, van Gogh's
images are close to the stereographic variant.

A Pannini perspective grid is illustrated, this has a 10\degree spacing, notice
that radial lines are equally spaced, vertical lines are straight and vertical,
but the spacing increases away from the centre, horizontal lines bow as is
necessary to maintain low local distortion.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image02.png}
  \caption{ \emph{Pannini perspective grid, by the author}.}
  \vspace{-3mm}
  \label{fig:pannini-grid}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/castle-square-pannini.jpg}
  \caption{ \emph{Same view as the author's test sketch, but as a Pannini projection photo}, by the author (2010).}
  \vspace{-3mm}
  \label{fig:pannini-castlesquare}
\end{figure}

This grid can be overlaid onto the artist's work, both The Bedroom in Arles and
The Night Café paintings can be closely matched to the Pannini grid (Figures
\ref{fig:bedroom-overlay}, \ref{fig:cafe-overlay-1}), \ref{fig:cafe-overlay-2},
in fact it is possible to read the angle-of-view of both paintings, the first
is 65\degree and the second is 110\degree.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image07.jpg}
  \caption{ \emph{Bedroom in Arles with Pannini overlay}, Vincent van Gogh (1888).}
  \vspace{-3mm}
  \label{fig:bedroom-overlay}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image06-overlay.png}
  \caption{ \emph{The Night Café with Pannini overlay}, Vincent van Gogh (1888).}
  \vspace{-3mm}
  \label{fig:cafe-overlay-1}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image10-overlay.png}
  \caption{ \emph{The Night Café with Pannini overlay}, Vincent van Gogh (1888).}
  \vspace{-3mm}
  \label{fig:cafe-overlay-2}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/Ward in the Hospital at Arles overlay.jpg}
  \caption{ \emph{Ward in the Hospital at Arles with Pannini overlay}, Vincent van Gogh (1889).}
  \vspace{-3mm}
  \label{fig:ward-overlay}
\end{figure}

We don't have any evidence that van Gogh's frame was used in either of these
paintings, indeed we don't know if it was ever modified to be suitable for use
on a hard floor.

\section{3D reconstruction}
\label{reconstruction}

TODO

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/vgb-90deg-above.png}
  \caption{ \emph{Bedroom in Arles reconstructed from plans, view from above}, by the author (2010)}
  \vspace{-3mm}
  \label{fig:bedroom-model-above}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/vgb-plan-crop.jpg}
  \caption{ \emph{Bedroom in Arles plan}, by XXXXX (XXXX) TODO}
  \vspace{-3mm}
  \label{fig:bedroom-plan}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/vgb-90deg-pannini_arles1.png}
  \caption{ \emph{Bedroom in Arles reconstructed from plans and rendered in Pannini projection}, by the author (2010)}
  \vspace{-3mm}
  \label{fig:bedroom-model-pannini}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/vgb-90deg-rectilinear.png}
  \caption{ \emph{Bedroom in Arles reconstructed from plans and rendered in standard rectilinear projection}, by the author (2010)}
  \vspace{-3mm}
  \label{fig:bedroom-model-rectilinear}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/De-slaapkamer-3D-kl.jpg}
  \caption{ \emph{Bedroom in Arles reconstructed in the van Gogh museum, Amsterdam}, \cite{deslaapkamer}}
  \vspace{-3mm}
  \label{fig:bedroom-photo}
\end{figure}

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/De-slaapkamer-3D-kl-panini.jpg}

  \caption{ \emph{Bedroom in Arles reconstructed in the van Gogh museum,
Amsterdam}, photograph from Figure \ref{fig:bedroom-photo}, converted to Pannini
projection by the author (2011)}

  \vspace{-3mm}
  \label{fig:bedroom-photo-pannini}
\end{figure}

\section{Photographic example}

The General Pannini projection has been added by the authors to the Hugin
panorama stitcher \cite{hugin}, with this is it possible to try and reproduce
these kind of images photographically. Here we show an example of a similar
scene to The Night Café, this has been photographed with multiple overlapping
shots since the angle-of-view required is considerably wider than a normal lens
can provide, the image shown has been simply assembled in Hugin and the
projection set to 'General Pannini' without any further adjustment other than
cropping.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{images/image04.jpg}
  \caption{ \emph{The Cobden View}, Pannini projection photograph by the author (2010).}
  \vspace{-3mm}
  \label{fig:cobden}
\end{figure}

(TODO similar photo exercise with bedroom)

\section{Conclusion}
\label{conclusion}

Very close correlation between Pannini projection and some paintings by the
artist. TODO more

Further research needs to look for the distinctive construction grid in
paintings and sketches by the artist.

\bibliography{van-gogh}

\end{document}

